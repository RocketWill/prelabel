# 预标注工具
将 COCO 格式的预测结果 json 文件结合 labelme 标注，输出 labelme 格式的预标注文件。目前仅支持 rectangle 框。

## Usage
```bash
python prelabel.py [coco_res] [dataset_dir] [output_dir]
```

### 参数说明
1. `coco_res`: COCO 格式的输出结果 json 文件
2. `dataset_dir`: 数据集文件夹 (包含 labelme 格式标注文件与图片)
3. `output_dir`: 预标注数据集输出文件夹