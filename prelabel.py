#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import json
import os
import ntpath
import glob
import tqdm
from argparse import ArgumentParser
from collections import defaultdict
from typing import List

def bbox_to_rectangle(bbox: List[float]) -> List[List[float]]:
    x, y, width, height = bbox
    return [[x, y], [x + width, y + height]]

def grp_by_img_name(coco_predict_res: str) -> defaultdict:
    img_name_dict = defaultdict(list)
    with open(coco_predict_res, 'r') as reader:
        dets = json.loads(reader.read())
        for det in dets:
            image_file = det['image_file']
            img_name_dict[image_file].append(det)
    return img_name_dict

def create_shape_obj(label_name: str, points: List[List[float]], shape_type: str="rectangle") -> dict:
    return {
        "label": label_name,
        "points": points,
        "group_id": None,
        "shape_type": shape_type,
        "flags": {}
    }

def read_labelme_file(labelme_anno_file: str) -> dict:
    with open(labelme_anno_file, 'r') as reader:
        labelme_obj = json.loads(reader.read())
    return labelme_obj

def write_json_to_file(obj: dict, output_path: str):
    with open(output_path, 'w') as outfile:
        json.dump(obj, outfile)

def copy_file(src: str, dest: str):
    cmd = "cp {} {}".format(src, dest)
    os.system(cmd)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("coco_res", help="COCO predict result json file path")
    parser.add_argument("dataset_dir", help="Directory contains labelme annotation files and images")
    parser.add_argument("output_dir", help="Output directory")
    args = parser.parse_args()

    dataset_dir = args.dataset_dir
    output_dir = args.output_dir

    img_name_dict = grp_by_img_name(args.coco_res)
    labelme_annos_files = glob.glob(os.path.join(dataset_dir, "*.json"))
    labelme_annos_files = [ntpath.basename(anno_file) for anno_file in labelme_annos_files]
    os.makedirs(output_dir, exist_ok=True)

    category_map = {
        0: "hat",
        1: "head",
        2: "person",
        3: "cell_phone",
        4: "cigarette",
        5: "flame"
    }

    for img_name, dets in tqdm.tqdm(img_name_dict.items()):
        json_file_name = img_name.rsplit(".", 1)[0] + '.json'
        if os.path.exists(os.path.join(dataset_dir, img_name)):
            if json_file_name in labelme_annos_files:
                anno_obj = read_labelme_file(os.path.join(dataset_dir, json_file_name))
                for det in dets:
                    label_name = category_map[det['category_id']]
                    points = bbox_to_rectangle(det['bbox'])
                    shape = create_shape_obj(label_name, points)
                    anno_obj['shapes'].append(shape)
                write_json_to_file(anno_obj, os.path.join(output_dir, json_file_name))
                copy_file(os.path.join(dataset_dir, img_name), output_dir)
            else:
                write_json_to_file(anno_obj, os.path.join(output_dir, json_file_name))
                copy_file(os.path.join(dataset_dir, img_name), output_dir)

